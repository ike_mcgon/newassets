﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour {
	public GameObject ball;
	private float count;
    private Vector3 position = Vector3.zero;

    // Use this for initialization
    void Start () {
		count = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time > count) {
            position = new Vector3(Random.Range(-0.5f, 0.5f), 3, Random.Range(-0.5f, 0.5f));

            Instantiate(ball, position, Quaternion.identity);
			count += Random.Range(1.0f, 2.5f);
		}
	}
}
