﻿using UnityEngine;

public class DestroyOnImpact : MonoBehaviour {
	private GameController gameController;

	// Use this for initialization
	void Start () { 
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <GameController>();
		}
	}
	
	void OnTriggerEnter(Collider other) {
		// If controller and grabbing, update score
        //Already won!
		gameController.AddCollision();
		Destroy(gameObject);
    }
}
