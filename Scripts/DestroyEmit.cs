﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEmit : MonoBehaviour {
    public float destroyTime = 0.5f;

	void Start () {
        Destroy(this.gameObject, destroyTime);
	}
}
