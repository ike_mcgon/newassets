﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickObject : MonoBehaviour {
    public GameObject FX;
    private Vector3 finalPos = Vector3.zero;

    void OnMouseDown()
    {
        Destroy(obj: gameObject);

        finalPos = transform.position;
        Instantiate(FX, finalPos, Quaternion.identity);
    }
}
