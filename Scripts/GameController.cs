﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int gameLength;
    public Text scoreText;
    public Text endText;
   
    private int collisions;
    private bool gameOver;


	void Start ()
	{
		collisions = 0;
        gameOver = false;
        if (gameLength < 10)
        {
            gameLength = 60;
        }
	}

    void Update()
    {
        if (Time.realtimeSinceStartup > gameLength && !gameOver)
        {
            EndGame();
        }
        if (gameOver) {
            GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
            if (balls.Length == 0) {
                endText.text = "Game Over!";
            }
        }
    }

    public void AddCollision ()
	{
		collisions += 1;
		Debug.Log("Collisions: " + collisions);
        scoreText.text = "Score: " + collisions;
	}

    private void EndGame()
    {
        Debug.Log("End of Game sequence.");
        gameOver = true;
        // Peform a scene switch or display a gui.

        GameObject spawner = GameObject.FindWithTag("Respawn");
        if (spawner != null)
        {
            Destroy(spawner);
        } else
        {
            Debug.Log("Spawner not found.");
        }
    }
}