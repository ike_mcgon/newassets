# VR AR Compare #

A project to compare the preference of patients between HTC VIVE and HOLOLENS. 

## Development ## 

Unity 5.6 

SteamVR Plugin 1.2.3

### Bugs ###

Vive controllers not showing. This seems to be a new bug with Unity 5.6

https://github.com/ValveSoftware/steamvr_unity_plugin/issues/66
